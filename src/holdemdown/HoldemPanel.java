package holdemdown;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JPanel;

/**
 * @author Andrew MacRae <liboff@gmail.com>
 *
 *
 */
public class HoldemPanel extends JPanel
{
    
    // tempo
    long globoQuadCount = 0;
    long globoRunCount = 0;
    long globoStraightFlushCount = 0;
    
    boolean runningSimulation = false;
    int globoCount = 1;
    // tiempo

    final int CALL = 2, FOLD = 1, DEAL = 0;
    int lastAction;
    Color[] chipCols;
    String analString;
    int numChips;
    int chipW, chipH;
    int heroChipX0, heroChipY0, betX0, betY0, potX0, potY0;
    double eVal;
    int[][] mask6 =
    {
        {
            1, 1, 1, 1, 1, 0
        },
        {
            1, 1, 1, 1, 0, 1
        },
        {
            1, 1, 1, 0, 1, 1
        },
        {
            1, 1, 0, 1, 1, 1
        },
        {
            1, 0, 1, 1, 1, 1
        },
        {
            0, 1, 1, 1, 1, 1
        }
    };
    int[][] mask =
    {
        {
            1, 1, 1, 1, 1, 0, 0
        },
        {
            1, 1, 1, 1, 0, 1, 0
        },
        {
            1, 1, 1, 0, 1, 1, 0
        },
        {
            1, 1, 0, 1, 1, 1, 0
        },
        {
            1, 0, 1, 1, 1, 1, 0
        },
        {
            0, 1, 1, 1, 1, 1, 0
        },
        {
            1, 1, 1, 1, 0, 0, 1
        },
        {
            1, 1, 1, 0, 1, 0, 1
        },
        {
            1, 1, 0, 1, 1, 0, 1
        },
        {
            1, 0, 1, 1, 1, 0, 1
        },
        {
            0, 1, 1, 1, 1, 0, 1
        },
        {
            1, 1, 1, 0, 0, 1, 1
        },
        {
            1, 1, 0, 1, 0, 1, 1
        },
        {
            1, 0, 1, 1, 0, 1, 1
        },
        {
            0, 1, 1, 1, 0, 1, 1
        },
        {
            1, 1, 0, 0, 1, 1, 1
        },
        {
            1, 0, 1, 0, 1, 1, 1
        },
        {
            0, 1, 1, 0, 1, 1, 1
        },
        {
            1, 0, 0, 1, 1, 1, 1
        },
        {
            0, 1, 0, 1, 1, 1, 1
        },
        {
            0, 0, 1, 1, 1, 1, 1
        }
    };

    int foldButtX, foldButtY, foldButtR;
    int callButtX, callButtY, callButtR;
    int dealButtX, dealButtY, dealButtR;

    int[] strengths = new int[21];
    Card[] heroHole, villainHole, community, comTurn;

    Card[] outs;
    int outIdx;

    Hand heroHand, villainHand;
    Deck deck;

    Image bgImage, rArrowImage, lArrowImage;
    Image foldButtUp, foldButtDown, callButtUp, callButtDown, dealButtUp, dealButtDown;

    int bgW, bgH;

    int playerWins;
    int heroHandStrength, villainHandStrength;

    boolean hideRiver;
    boolean loaded;
    boolean showOuts;

    int pot, bet, bigBlind;
    int heroStack, villainStack;
    int sklanskyBucks = 0;

    int handsPlayed = 0;

    public HoldemPanel()
    {
        showOuts = false;
        callButtR = 75 / 2;
        foldButtR = callButtR;
        dealButtR = 125 / 2;
        callButtX = 290;
        callButtY = 405;
        dealButtX = 370;
        dealButtY = callButtY - 25;
        foldButtX = callButtX + 2 * callButtR + 2 * dealButtR + 11;
        foldButtY = callButtY;
        analString = "";
        chipW = 30;
        chipH = 6;
        heroChipX0 = 5;
        heroChipY0 = 270;
        betX0 = 400;
        betY0 = 100;
        potX0 = 600;
        potY0 = 400;

        outs = new Card[52];
        outIdx = 0;

        bigBlind = 2;
        heroStack = 100;
        villainStack = 100;
        hideRiver = false;
        heroHand = new Hand();
        villainHand = new Hand();
        deck = new Deck();
        heroHole = new Card[2];
        villainHole = new Card[2];
        community = new Card[5];
        comTurn = new Card[4];
        for (int i = 0; i < 2; i++)
        {
            heroHole[i] = new Card();
            villainHole[i] = new Card();
        }
        for (int j = 0; j < 5; j++)
        {
            community[j] = new Card();
        }
        for (int j = 0; j < 4; j++)
        {
            comTurn[j] = new Card();
        }
        for (int j = 0; j < outs.length; j++)
        {
            outs[j] = new Card();
        }
        deck.shuffle();
        try
        {
            bgImage = ImageIO.read(new File("Images/board_bg.png"));
            rArrowImage = ImageIO.read(new File("Images/right_arrow.png"));
            lArrowImage = ImageIO.read(new File("Images/left_arrow.png"));
            callButtUp = ImageIO.read(new File("Images/Call_UP.png"));
            callButtDown = ImageIO.read(new File("Images/Call_DOWN.png"));
            foldButtUp = ImageIO.read(new File("Images/Fold_UP.png"));
            foldButtDown = ImageIO.read(new File("Images/Fold_DOWN.png"));
            dealButtUp = ImageIO.read(new File("Images/Deal_UP.png"));
            dealButtDown = ImageIO.read(new File("Images/Deal_DOWN.png"));
        }
        catch (IOException e)
        {
            System.out.println("Error loading background image");
            e.printStackTrace();
        }
        bgW = bgH = 256;
        playerWins = 0;
        pot = (int) (Math.random() * 100 + 50);
        bet = (int) (Math.random() * 50 + 1);
        hideRiver = true;
        do
        {
            turn();
//            System.out.print(".");
        }
        while (heroHand.compare(villainHand) >= 0);
        outs();
//        System.out.print("\n");
        loaded = true;

        numChips = 5;
        chipCols = new Color[numChips];
        chipCols[0] = Color.BLACK;
        chipCols[1] = Color.BLUE;
        chipCols[2] = Color.GREEN;
        chipCols[3] = Color.RED;
        chipCols[4] = Color.WHITE;
    }

    @Override
    public void paintComponent(Graphics g)
    {
        for (int i = 0; i < 4; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                g.drawImage(bgImage, i * 256, j * 256, this);
            }
        }

        for (int i = 0; i < heroHole.length; i++)
        {
            heroHole[i].draw(g, 25 + i * (10 + heroHole[0].W + 5), 20);
            villainHole[i].draw(g, 600 + i * (10 + heroHole[0].W + 5), 20);
        }

        if (hideRiver)
        {
            for (int j = 0; j < 4; j++)
            {
                comTurn[j].draw(g, 140 + j * (10 + comTurn[0].W + 5), 220);
            }
            g.drawString("Pot : " + pot, potX0, potY0 - 20);
            g.drawString("Bet : " + bet, 400, 75);
            g.drawImage(callButtUp, callButtX, callButtY, this);
            g.drawImage(foldButtUp, foldButtX, foldButtY, this);
            g.drawImage(dealButtDown, dealButtX, dealButtY, this);

        }
        else
        { // player called here

            for (int j = 0; j < 5; j++)
            {
                community[j].draw(g, 140 + j * (10 + community[0].W + 5), 220);
            }
            g.drawImage(callButtDown, callButtX, callButtY, this);
            g.drawImage(foldButtDown, foldButtX, foldButtY, this);
            g.drawImage(dealButtUp, dealButtX, dealButtY, this);

        }
        Font font = new Font("Jokerman", Font.PLAIN, 26);
        g.setFont(font);
        g.setColor(Color.YELLOW);
        g.drawString(Hand.handRankToString(heroHand.handRank()), 80, 200);
        g.drawString(Hand.handRankToString(villainHand.handRank()), 655, 200);
        if (lastAction == CALL)
        {
            if (heroHand.compare(villainHand) < 0)
            {
                if (!loaded)
                {
                    g.drawImage(rArrowImage, 290, 50, this);
                    g.setColor(Color.red);
                    g.drawString("You lose! Villain takes " + pot, 280, 570);
                }
            }
            else
            {
                if (heroHand.compare(villainHand) > 0)
                {
                    if (!loaded)
                    {
                        g.drawImage(lArrowImage, 290, 50, this);
                        g.setColor(Color.yellow);                        
                        g.drawString("You win! Hero takes " + pot, 280, 570);
                        heroStack += pot;
                    }
                }
                else
                {
                    g.drawString("DRAW!!!!", 370, 60);
                    heroStack += pot / 2;
                }
            } 
            //printAnalysis(int nbrOuts, int bet, int pot,boolean call)
            printAnalysis(outIdx,bet,pot,true);
        }
        else if(lastAction == FOLD)
        {
            printAnalysis(outIdx,bet,pot,false);
        }

        g.drawString("Stack: " + heroStack, 5, 250);

        if (showOuts)
        {
            for (int i = 0; i < outIdx; i++)
            {
                outs[i].draw(g, 85 + i * 40, 585, 80, 120);
            }
        }
// Draw player stack        
        int[] tmp = denominations(heroStack);

        int xx = heroChipX0;
        int yy = heroChipY0;
        int th = chipH;
        int tw = chipW;

        for (int i = 0; i < tmp.length; i++)
        {
            g.setColor(chipCols[i]);
            for (int j = 0; j < tmp[i]; j++)
            {
                g.fillRect(xx, yy, tw, th);
                yy += th + 1;
            }
            if (tmp[i] > 0)
            {
                xx += chipW + 1;
            }
            yy = heroChipY0;
        }
        if (hideRiver)
        {
            tmp = denominations(bet);
            xx = betX0;
            yy = betY0;
            for (int i = 0; i < tmp.length; i++)
            {
                g.setColor(chipCols[i]);
                for (int j = 0; j < tmp[i]; j++)
                {
                    g.fillRect(xx, yy, tw, th);
                    yy += th + 1;
                }
                if (tmp[i] > 0)
                {
                    xx += chipW + 1;
                }
                yy = betY0;
            }

            tmp = denominations(pot);
            xx = potX0;
            yy = potY0;
            for (int i = 0; i < tmp.length; i++)
            {
                g.setColor(chipCols[i]);
                for (int j = 0; j < tmp[i]; j++)
                {
                    g.fillRect(xx, yy, tw, th);
                    yy += th + 1;
                }

                if (tmp[i] > 0)
                {
                    xx += chipW + 1;
                }
                yy = potY0;
            }

        }

    }

    public void handleInput(char code, KeyEvent e)
    {
        switch (code)
        {
            case 'p': // Key Pressed
                if (e.getKeyCode() == KeyEvent.VK_UP)
                {
                }
                break;
            case 'r':  // Released
                break;
            case ('t'): // Typed
                char c = Character.toLowerCase(e.getKeyChar());
                switch (c)
                {
                    case 'f':
                        break;
                    case 'c':
                        break;

                    case 'd':
                        deal();
                        break;
                    case 's':
                        if (runningSimulation)
                        {
                            runningSimulation = false;
                        }
                        else
                        {
                            runningSimulation = true;
                            runSimulation();
                        }
                        break;
                    case 't':
                        break;
                }
                break;
        }
    }

    public void handleMouse(char code, MouseEvent e)
    {
        switch (code)
        {
            case 'd':
                break;
            case 'p':
                break;
            case 'r':
                break;
            case 'c':
                int butt = isButton(e.getX(), e.getY());
                if (butt == CALL)
                {

                    if (loaded)
                    {
                        doCall();
                    }
                }
                else
                {
                    if (butt == FOLD)
                    {
                        if (loaded)
                        {
                            doFold();
                        }
                    }
                    else
                    {
                        if (butt == DEAL && !hideRiver)
                        {
                            doDeal();
                        }
                    }
                }

                break;
        }
    }

    private void doCall()
    {
        lastAction = CALL;
        showOuts = true;
        if (eVal > 0)
        {
            analString = "Good Call: With " + outIdx + " outs, you had an EV of " + truncate(eVal, 2);
        }
        else
        {
            analString = "Poor call ... With " + outIdx + " outs, your EV was " + truncate(eVal, 2);
        }
        sklanskyBucks += eVal;
        heroStack -= bet;
        pot += bet;
        showRiver();

        hideRiver = false;
        handsPlayed++;
        loaded = false;
        repaint();
    }

    private void doFold()
    {
        lastAction = FOLD;
        showOuts = true;
        if (eVal > 0)
        {
            analString = "Bad Fold: With " + outIdx + " outs, you had an EV of " + truncate(eVal, 2);
        }
        else
        {
            analString = "Good fold ... with " + outIdx + " outs, your EV was " + truncate(eVal, 2);
        }

        heroStack -= bigBlind;

        showRiver();
        loaded = false;
        hideRiver = false;
        handsPlayed++;

        repaint();
    }

    private boolean doDeal()
    {
        lastAction = DEAL;
        analString = "";
        pot = (int) (Math.random() * 100 + 50);
        bet = (int) (Math.random() * 50 + 1);
        hideRiver = true;
        do
        {
            turn();
//            System.out.print(".");
        }
        while (heroHand.compare(villainHand) >= 0);
//        System.out.print("\n");
        loaded = true;
        showOuts = false;
        outs();
        repaint();
        return true;
    }

    public void deal()
    {
        hideRiver = false;
        deck.shuffle();
        for (int i = 0; i < 2; i++)
        {
            heroHole[i].copy(deck.deal());
            villainHole[i].copy(deck.deal());
        }
        for (int j = 0; j < 5; j++)
        {
            community[j].copy(deck.deal());
        }
        villainHand.copy(evaluate(append(villainHole, community)));
        heroHand.copy(evaluate(append(heroHole, community)));
        repaint();
    }

    public void turn()
    {
        hideRiver = true;
        deck.shuffle();
        for (int i = 0; i < 2; i++)
        {
            heroHole[i].copy(deck.deal());
            villainHole[i].copy(deck.deal());
        }
        for (int j = 0; j < 4; j++)
        {
            comTurn[j].copy(deck.deal());
        }

        villainHand.copy(evaluate(append(villainHole, comTurn)));
        heroHand.copy(evaluate(append(heroHole, comTurn)));
        repaint();
    }

    public Hand evaluate(Card[] cards)
    {
        int len = strengths.length;
        boolean do7 = false;
        if (cards.length == 7)
        {
            do7 = true;
            len = 21;
        }
        else
        {
            if (cards.length == 6)
            {
                do7 = false;
                len = 6;
            }
            else
            {
                System.out.println("Error: Incorrect card length in evaluate()");
                System.exit(-1);
            }
        }
        Hand hand = new Hand(); // temporary holding hand
        Hand ret = new Hand();
        int mx = -1;

        for (int i = 0; i < len; i++)
        {
            hand.reset();
            if (do7)
            {
                for (int j = 0; j < mask[0].length; j++)
                {
                    if (mask[i][j] == 1)
                    {
                        hand.addCard(cards[j]);
                    }
                }
                strengths[i] = hand.handRank();

                if (strengths[i] > mx)
                {
                    mx = strengths[i];
                    ret.copy(hand);
                }
                else
                {
                    if (strengths[i] == mx)
                    {
                        if (hand.compare(ret) > 0)
                        {
                            ret.copy(hand);
                        }
                    }
                }
            }
            else
            {
                for (int j = 0; j < mask6[0].length; j++)
                {
                    if (mask6[i][j] == 1)
                    {
                        hand.addCard(cards[j]);
                    }
                }
                strengths[i] = hand.handRank();

                if (strengths[i] > mx)
                {
                    mx = strengths[i];
                    ret.copy(hand);
                }
                else
                {
                    if (strengths[i] == mx)
                    {
                        if (hand.compare(ret) > 0)
                        {
                            ret.copy(hand);
                        }
                    }
                }
            }
        }
        return ret;
    }

    public Card[] append(Card[] c1, Card[] c2)
    {
        int lTot = c1.length + c2.length;
        Card[] ret = new Card[lTot];
        for (int i = 0; i < c1.length; i++)
        {
            ret[i] = new Card();
            ret[i].copy(c1[i]);
        }
        for (int i = c1.length; i < lTot; i++)
        {
            ret[i] = new Card();
            ret[i].copy(c2[i - c1.length]);
        }
        return ret;
    }

    private boolean showRiver()
    {
        if (!loaded)
        {
            System.out.println("Error: turn isn't dealt - can't show river...");
            return false;
        }
        loaded = false;
        for (int i = 0; i < 4; i++)
        {
            community[i].copy(comTurn[i]);
        }

        villainHand.copy(evaluate(append(villainHole, community)));
        heroHand.copy(evaluate(append(heroHole, community)));
        repaint();
        return true;
    }

    private int isButton(int x, int y)
    {
        if (dist(x, y, callButtX + callButtR, callButtY + callButtR) <= callButtR)
        {
            return CALL;
        }
        else
        {
            if (dist(x, y, foldButtX + foldButtR, foldButtY + foldButtR) <= foldButtR)
            {
                return FOLD;
            }
            else
            {
                if (dist(x, y, dealButtX + dealButtR, dealButtY + dealButtR) <= dealButtR)
                {
                    return DEAL;
                }
                else
                {
                    return -1;
                }
            }
        }
    }

    private int dist(int x1, int y1, int x2, int y2)
    {
        return (int) Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
    }

    int[] denominations(int i)
    {
        //1,2,5,10,25,100
        int[] ret = new int[numChips];
        for (int j = 0; j < ret.length; j++)
        {
            ret[j] = 0;
        }
        while (i > 25)
        {
            i -= 25;
            ret[0]++;
        }
        while (i > 10)
        {
            i -= 10;
            ret[1]++;
        }
        while (i > 5)
        {
            i -= 5;
            ret[2]++;
        }
        while (i > 2)
        {
            i -= 2;
            ret[3]++;
        }
        while (i >= 1)
        {
            i--;
            ret[4]++;
        }

        return ret;
    }

    int outs()
    {
        long t0 = System.currentTimeMillis();
        outIdx = 0;
        Card[] tmp = new Card[1];
        int heroOuts = 0;
        int villainOuts = 0;
        int drawingOuts = 0;

// DO WHILE here!        
        tmp[0] = deck.deal();
        community[4].copy(deck.deal());
        int cf = evaluate(append(append(heroHole, comTurn), tmp)).compare(evaluate(append(append(villainHole, comTurn), tmp)));
        if (cf > 0)
        {
            outs[outIdx].copy(tmp[0]);
            heroOuts++;
            outIdx++;
        }
        else
        {
            if (cf < 0)
            {
                villainOuts++;
            }
            else
            {
                drawingOuts++;
            }
        }

        while (!deck.isEmpty())
        {
            tmp[0] = deck.deal();
            cf = evaluate(append(append(heroHole, comTurn), tmp)).compare(evaluate(append(append(villainHole, comTurn), tmp)));
            if (cf > 0)
            {
                outs[outIdx].copy(tmp[0]);
                heroOuts++;
                outIdx++;
            }
            else
            {
                if (cf < 0)
                {
                    villainOuts++;
                }
                else
                {
                    drawingOuts++;
                }
            }
        }
        double otp = outsToPct(heroOuts);
        eVal = (otp * (bet + pot) - bet*(1-otp));
//        System.out.println("Hero outs: " + heroOuts + ". Pot = " + pot + ", bet = " + bet + ". EV to call: " + eVal);
//        for (int i = 0; i < outIdx; i++)
//        {
//            System.out.print(outs[i].toString() + " ");
//        }
//        System.out.println("\nCalculated outs in " + (System.currentTimeMillis() - t0) + "ms.");
        return heroOuts;
    }

    public double outsToPct(int outs)
    {
        return (double) outs * 0.02;
    }

    double truncate(double f, int d)
    {
        return (double) ((int) (Math.pow(10, d) * f)) / Math.pow(10, d);
    }


    private void runSimulation()
    {
        Thread t1 = new Thread(new Runnable()
        {
            public void run()
            {
                BufferedWriter buffy = null;
                int tmpcnt = 0;
                int colMax = 1000000;
                    System.out.print("Player "+globoCount+": ");
                    while (runningSimulation)
                    {
                        try
                        {
                            tmpcnt++;       
                            Thread.sleep(125);
                            doDeal();
                            while(!loaded) Thread.sleep(125);                            
                            if (eVal > 0)
                            {
                                doCall();
                            }
                            else
                            {
                                doFold();
                            }

                            if(tmpcnt%(colMax/10)==0) System.out.print("-");
//                            if(tmpcnt>colMax)
//                            {                                
//                                globoCount++;
//                                System.out.print("\nPlayer "+globoCount+":\t");
//
//                                tmpcnt=0;
//                                heroStack = 100;
//                                //System.out.println("Done "+globoCount+ " players.");
//
//                            }

                        }
                        catch (InterruptedException ex)
                        {
                            Logger.getLogger(HoldemPanel.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }

                }

        });
        t1.start();
    }
    
    private void check4monsters()
    {
        globoRunCount++;
        if (villainHand.isQuads())
        {
            globoQuadCount++;
            System.out.println("OMFG monstar!!!!!!");
            System.out.println("Hand: " + villainHand.toString());
            System.out.println("QuadCount: "+globoQuadCount+":"+globoRunCount);
        }
        if (villainHand.isStraight() && villainHand.isFlush())
        {
            System.out.println("OMFG monstarrrrrr!!!!!!");
            System.out.println("Hand: " + villainHand.toString());
            System.out.println("QuadCount: "+globoQuadCount+":"+globoRunCount);
            
            globoStraightFlushCount++;
            System.out.println("StraightFlushCount: "+globoStraightFlushCount+":"+globoRunCount);
        }
        if (heroHand.isQuads())
        {

            System.out.println("OMFG monster!!!!!!");
            System.out.println("Hand: " + heroHand.toString());
            globoQuadCount++;
            System.out.println("QuadCount: "+globoQuadCount+":"+globoRunCount);
        }
        if (heroHand.isStraight() && heroHand.isFlush())
        {
            System.out.println("OMFG monsterrrrrr!!!!!!");
            System.out.println("Hand: " + heroHand.toString());
            globoStraightFlushCount++;
            System.out.println("StraightFlushCount: "+globoStraightFlushCount+":"+globoRunCount);
        }
        
    }
    
    void printAnalysis(int nbrOuts, int bet, int pot,boolean call)
    {
        double beta = (double)bet/(double)pot;
        double eta = outsToPct(nbrOuts);
        double bMax;
        if(eta<0.5)
        {
            bMax = eta/(1-2*eta);
        }
        else
        {
            bMax = 1e5;
        }
        if(call)
        {
            if(nbrOuts==0)
            {
                System.out.println("Incorrect call. You had no outs and were drawing dead!");                
            }
            else if(beta<=bMax)
            {
                System.out.println("Correct call!\nYou had "+nbrOuts+" outs and called a bet of $"+bet);
                System.out.println("It was profitable to call any bet up to $"+((float)((int)(100*pot*bMax)))/100);
            }
            else
            {
                System.out.println("Incorrect call!\nYou had "+nbrOuts+" outs and called a $"+bet+" bet to win $"+(pot+bet));
                System.out.println("It was only profitable to call a bet as large as $"+((float)((int)(100*pot*bMax)))/100);
            }            
        }
        else
        {
            if(nbrOuts==0)
            {
                System.out.println("Correct fold! You had no outs and were drawing dead.");                
            }
            else if(beta<=bMax)
            {
                System.out.println("Incorrect fold!\nYou had "+nbrOuts+" outs and folded to a $"+bet+" bet to win $"+(pot+bet));
                System.out.println("It was profitable to call a bet as large as $"+((float)((int)(100*pot*bMax)))/100);
            }
            else
            {
                System.out.println("Correct fold!\nYou had "+nbrOuts+" outs and folded $"+bet+" to win $"+(bet+pot));
                System.out.println("It was only profitable to call any bet up to $"+((float)((int)(100*pot*bMax)))/100);
            }            
        }
        System.out.println("Real bucks: $"+heroStack);
        System.out.println("Sklansky bucks: $"+sklanskyBucks+"\n");
    }
    
}
