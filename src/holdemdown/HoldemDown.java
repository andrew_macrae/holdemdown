/*
 *  This software is free.
 */
package holdemdown;

/*
 *  Conventions: 
 *      Suit ordering: C == 1, D == 2, H == 3, S == 4
 *      Rank definition 2-9 == '2-9', 10 == 't' J == '11', Q == '12', K == '13', A == '14'
 */
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;

/**
 * 
 * @author amacrae
 * 
 *  Investigating putting a console into the program. If it's not made really hard by java, do it.
 */
public class HoldemDown extends JFrame implements KeyListener, MouseListener, MouseMotionListener
{

    HoldemPanel nPanel = new HoldemPanel();
    String temp;
    
    public static void main(String[] args)
    {
        HoldemDown mf = new HoldemDown();
        mf.setVisible(true);
        mf.init();
    }

    public HoldemDown()
    {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(850, 740);
        setTitle(" Hold'em Down ");
        Container content = getContentPane();
        content.add(nPanel, BorderLayout.CENTER);        
    }

    public void keyPressed(KeyEvent k)
    {
        nPanel.handleInput('p', k);
    }

    public void keyReleased(KeyEvent k)
    {
        nPanel.handleInput('r', k);
    }

    public void keyTyped(KeyEvent k)
    {
        nPanel.handleInput('t', k);
    }

    public void mousePressed(MouseEvent e)
    {
        nPanel.handleMouse('p', e);
    }

    public void mouseReleased(MouseEvent e)
    {
        nPanel.handleMouse('r', e);
    }

    public void mouseClicked(MouseEvent e)
    {
        nPanel.handleMouse('c', e);
    }

    public void mouseEntered(MouseEvent e)
    {
    }

    public void mouseExited(MouseEvent e)
    {
    }

    public void mouseMoved(MouseEvent e)
    {
        nPanel.handleMouse('m', e);        
    }

    public void mouseDragged(MouseEvent e)
    {
        nPanel.handleMouse('d', e);
    }

    public void init()
    {
        nPanel.setFocusable(true);
        nPanel.addKeyListener(this);
        nPanel.addMouseListener(this);
        nPanel.addMouseMotionListener(this);
    }
}
