/*
 *  This software is free.
 */

package holdemdown;

public class Deck 
{
    Card[] cards;
    int cardsOut;
    int cardWidth = 100;
    int cardHeight = (int)(1.5*cardWidth);
    
    public Deck()
    {
        cards = new Card[52];
        initDeck();
        cardsOut = 0;
    }
    
    private void initDeck()
    {
        for(int i = 0;i<13;i++)
        {
            for(int j = 0;j<4;j++)
            {
                cards[4*i+j] = new Card(0,0,cardWidth,cardHeight,Card.intToRank(i+2),Card.intToSuit(j+1));
                //System.out.println("Created card "+cards[4*i+j].rank()+","+cards[4*i+j].suit()+". rankToInt() returns: "+ cards[4*i+j].rankToInt());
            }
        }
    }
    
// Fisher–Yates shuffle algorithm    
    public void shuffle()
    {
        cardsOut = 0;
        for(int j = 51;j>0;j--)
        {
            int rdm = (int)(Math.random()*(j+1));
            swap(j,rdm);
        }
    }
    
    private void swap(int i,int j)
    {
        Card tmp = cards[i];
        cards[i] = cards[j];
        cards[j] = tmp;        
    }
    
    public Card deal()
    {
        if(cardsOut<52)
        {
            cardsOut++;
            return cards[cardsOut-1];
        }
        else return null;
    }
    
    public boolean isEmpty()
    {
        return cardsOut==cards.length;        
    }
}