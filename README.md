# README #

This is a standalone java application which I made to practice quickly calculating pot odds in Texas Hold'em poker. The setup is that you are heads up against an opponent, have been shown the flop and the turn, and are facing a bet. The catch is (a) you can always see your opponents cards and (b) you are always behind. The point is that you can still make profitable decisions when you think you are behind if the pot/bet size ratio is favourable. On each round you must either fold and lose $2 or call the bet, potentially losing the bet amount but potentially winning the pot. 

There are bad calls ( $45 to call a $100 pot with 3 outs), or good calls, ($5 to call $90 with 10 outs). 

The goal is to make as much money as possible and avoid going broke. Future versions should they ever exist, will have more features and streamlined gameplay.

Currently the poker engine is something I wrote on the fly with the aim of getting something that works up and running as quickly as possible. It is slow, ungainly, and should be replaced eventually.

### How do I get set up? ###

Either compile the source in Netbeans or run the jar file directly.

NOTE ... the Images folder must be in the same directory as the jar file to work properly.

### Contribution guidelines ###

### Who do I talk to? ###

Andrew MacRae (liboff@gmail.com)